from time_stamp import TimeStamp

class FileWriter:
    @staticmethod
    def get_name(tag):
        return "../output/" + tag + "_" + TimeStamp.get() + ".record"
    @staticmethod
    def write_array(name, array):
        file = open(name,"w")
        for x in array:
            file.write(x + "\n")
        file.close()
