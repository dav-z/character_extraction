from tokenizer import WordTokenizer, SentenceTokenizer
from normalizer import Normalizer
from word_statistics import WordStatistics
from scorer import Scorer

# Global Methods
def read_text(filepath):
    with open(filepath, 'r') as f:
        b = f.read()
    f.close()
    return b

# Data
words = {}

# Read Book
#raw_book = read_text("../data/cleaned/time_machine.txt")
#raw_book = read_text("../data/cleaned/adventures_of_huckleberry_finn.txt")
#raw_book = read_text("../data/cleaned/bible.txt")
#raw_book = read_text("../data/cleaned/bible_10000_lines.txt")
raw_book = read_text("../data/cleaned/genesis.txt")

# Normalize Book
normalized_book = Normalizer.normalize(raw_book)

# Tokenize Sentences
sentenceTokens = SentenceTokenizer.tokenize(normalized_book)

# Counting total document size
document_size = 0

# Tokenize Words within each sentence
for sentence in sentenceTokens:
    wordTokens = WordTokenizer.tokenize(sentence)
    index = 0
    for word in wordTokens:
        document_size += 1
        word_lower = word.lower()
        if word_lower in words:
            words[word_lower].update(wordTokens, index)
        else:
            words[word_lower] = WordStatistics(word_lower)
            words[word_lower].update(wordTokens, index)
        index += 1

# Compute an aggregate score between 0 and 1 for each word in words
word_scores = {}
for word in words:
    word_stats = words[word]
    individualScore1 = word_stats.individualScore1
    individualScore2 = word_stats.individualScore2
    globalScore1 = Scorer.frequencyScore(word_stats, document_size)
    globalScore2 = Scorer.precededByScore(word_stats)
    globalScore3 = Scorer.beginningScore(word_stats)
    globalScore4 = Scorer.capitalizationScore(word_stats)
    globalScore5 = Scorer.capitalizedNeighborsScore(word_stats)
    aggregateScore1 = (0.5 * individualScore1 + 0.3 * individualScore2 + 0.2 * globalScore1)
    aggregateScore2 = (0.8 + 0.2 * globalScore1) * (0.3 * globalScore2 + 0.1 * globalScore3 + 0.475 * globalScore4 + 0.125 * globalScore5)
    #word_scores[word] = (0.5 * aggregateScore1 + 0.5 * aggregateScore2)
    #aggregateScore3 = 0.2 * individualScore1 + 0.4 * individualScore2 + 0.4 * globalScore4
    #word_scores[word] = 0.01 * aggregateScore1 + 0.09 * aggregateScore2 + 0.9 * aggregateScore3
    filterScore = individualScore1 * individualScore2 * (globalScore4 == 1.0) * (0.5 * aggregateScore1 + 0.5 * aggregateScore2)
    # filterScore2 weights were computed using linear regression
    filterScore2 = individualScore1 * individualScore2 * (globalScore4 == 1.0) * (15.8600162 * globalScore1 + 0.820136521 * globalScore2 - 0.119725214 * globalScore3 + 0.0508475736 * globalScore5) / 16.7310002946
    word_scores[word] = [word, filterScore, individualScore1, individualScore2, globalScore1, globalScore2, globalScore3, globalScore4, globalScore5]
# Return the top scoring words in order
for item in sorted(word_scores.items(), key=lambda x: -x[1][1]):
    print(item[1])
