# -*- coding: utf-8 -*-

import re

class Tokenizer():
    @staticmethod
    def tokenize(data):
        pass

class SentenceTokenizer(Tokenizer):
    @staticmethod
    def tokenize(data):
        ## https://regex101.com/r/rT3iZ4/5 ((?![.\n\s])[^.\n\"]*(?:\"[^\n\"]*[^\n\".]\"[^.\n\"]*)*(?:\"[^\"\n]+\.\"|\.|(?=\n)))
        ## Mike's Regex [ \n\r\.!?]*[\n\r\.!?]+[ \n\r\.!?]*
        return re.split(r"[ \n\r\.!?]*[\n\r\.!?]+[ \n\r\.!?]*", data)

class WordTokenizer(Tokenizer):
    @staticmethod
    def tokenize(data):
        return re.findall(r"(?:[ÀÁÄÅÃÇÈÉÊÍÏÑÓÚÜßæàáâäçèéêëìíîïñòóôöùúûüABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz]+)(?:[-][ÀÁÄÅÃÇÈÉÊÍÏÑÓÚÜßàáâäçèéêëìíîïñòóôöùúûüABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz]+)?(?:['][ÀÁÄÅÃÇÈÉÊÍÏÑÓÚÜßàáâäçèéêëìíîïñòóôöùúûüABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz]{0,3})?", data)
