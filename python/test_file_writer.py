from file_writer import FileWriter

fruit_array = ["apple", "orange", "grape", "cherry"]
filename = FileWriter.get_name("fruit")
FileWriter.write_array(filename, fruit_array)
