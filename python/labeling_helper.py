from file_writer import FileWriter

wordMap = {}

with open('../huck_finn_filter1.txt') as file1:
    for line in file1:
        list = eval(line)
        word = list[0]
        label = list[9]
        wordMap[word] = label
file1.close()

newList = []

with open('../huck_finn_filter2.txt') as file2:
    for line in file2:
        list = eval(line)
        word = list[0]
        list.append(wordMap[word])
        newList.append(str(list))
file2.close()

FileWriter.write_array('../huck_finn_filter2_updated.txt', newList)

