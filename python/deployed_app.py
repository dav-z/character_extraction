from flask import Flask, request, render_template, jsonify
from lce_server import LCEServer

app = Flask(__name__)
# text = "hi hi hi"

@app.route('/')
def home():
    return render_template("index.html")

@app.route("/script/", methods=['GET', 'POST'])
def api_call():
    # input_string = request.form['data']
    input_string = request.form['book']
    response = LCEServer.handle_request(input_string)
    return jsonify(response)
