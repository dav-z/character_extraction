import csv
import numpy as np
from sklearn.linear_model import LinearRegression

# STEP 1: READ DATA FROM FILE
def read_data(filename):
    X = []
    Y = []
    with open(filename) as data_file:
        data_accessor = csv.reader(data_file, delimiter=',')        
        for row in data_accessor:
            i = (len(row)-1)
            X.append(row[1:i])
            Y.append(row[i])
        return [X, Y]

data = read_data("../../small_dataset.csv")
#print(str(data[1]))

# STEP 2: TRAIN MODEL
X = data[0]
Y = data[1]
reg = LinearRegression().fit(X, Y)

print(str(reg.coef_))
print(str(reg.intercept_))
