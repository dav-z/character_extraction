from scorer import Scorer

class WordStatistics():
    def __init__(self, word):
        self.word = word
        self.individualScore1 = Scorer.lengthScore(word)
        self.individualScore2 = Scorer.nonCommonScore(word)
        self.count1 = 0.0 # total
        self.count2 = 0.0 # after mr, mrs
        self.count3 = 0.0 # beginning of sentence
        self.count4 = 0.0 # capitalized
        self.count5 = 0.0 # before or after capitalized word

    def update(self, wordTokens, index):
        # count1
        self.count1 += 1.0

        # count2
        if index != 0:
            if wordTokens[index-1].lower() == "mister" or wordTokens[index-1].lower() == "missus":
                self.count2 += 1.0

        # count3
        if index == 0:
            self.count3 += 1.0

        # count4
        if wordTokens[index].istitle():
            self.count4 += 1.0

        # count5
        if (index != 0 and wordTokens[index-1].istitle()) or (index != len(wordTokens)-1 and wordTokens[index+1].istitle()):
            self.count5 += 1.0
