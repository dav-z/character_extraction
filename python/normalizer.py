# -*- coding: utf-8 -*-

import re

class Normalizer():
    def __init__(self, book):
        with open('../data/cleaned/time_machine_normalized.txt', 'w') as newfile:
            newfile.write(Normalizer.normalize(book))
    @staticmethod
    def normalize(book):
        return re.sub(r'[\"”“‟❝❞〝〞＂_)(]', '', book).replace("Mrs.", "Missus").replace("Ms.", "Missus").replace("Mr.", "Mister")

