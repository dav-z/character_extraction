##frequency test

from collections import Counter
from nltk import pos_tag, word_tokenize


def read_text():
    with open('../data/cleaned/time_machine.txt', 'r') as f:
        book = f.read()
    f.close()
    return book

def text_tokenize(book):
    tokenize = word_tokenize(book)
    words = [word for word in tokenize if word.isalpha()]
    return words

def tagging(words):
    tagged_text = pos_tag(words)
    return tagged_text


def find_proper_nouns(tagged_text):
    proper_nouns = []
    i = 0
    while i < len(tagged_text):
        if tagged_text[i][1] == 'NNP':
            if tagged_text[i+1][1] == 'NNP':
                proper_nouns.append(tagged_text[i][0] + " " + tagged_text[i+1][0])
                i+=1
            else:
                proper_nouns.append(tagged_text[i][0])
        i+=1
    return proper_nouns

def summarize_text(proper_nouns):
    counts = Counter(proper_nouns)
    return counts

b = read_text()
tok = text_tokenize(b)
ttt = tagging(tok)
prop = find_proper_nouns(ttt)
Nouns = summarize_text(prop)

# print(tok)
for word, freq in Nouns.most_common():
    print('%d %s' % (freq, word))

