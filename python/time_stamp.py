import time
import datetime

class TimeStamp:
    @staticmethod
    def get():
        return datetime.datetime.fromtimestamp(time.time()).strftime('%m_%d_%Y_%H_%M_%S')
